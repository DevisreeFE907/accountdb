package com.example.demo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
@Autowired
AccountRepository accountrepository;
public void save(Account account)
{
	accountrepository.save(account);
	System.out.println(account);
}
public List<Account> getAccount()
{
	return accountrepository.findAll();
}
}
